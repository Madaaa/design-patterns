package com.workshop.designpatterns.structural.adapter;

public interface InternalUser {
    String getUserName();

}
