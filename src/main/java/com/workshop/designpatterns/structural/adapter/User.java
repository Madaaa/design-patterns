package com.workshop.designpatterns.structural.adapter;

public class User implements InternalUser {
    private String userName;

    public User(String userName) {
        this.userName = userName;
    }

    @Override
    public String getUserName() {
        return userName;
    }
}
