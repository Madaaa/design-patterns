package com.workshop.designpatterns.structural.adapter;

public class AdapterDemo {
    public static void main(String[] args) {
        InternalUser user = new User("john_doe");
        System.out.println(user.getUserName());
        user = new FacebookUserAdapter(new FacebookUser("Sansa", "Doe"));
        System.out.println(user.getUserName());
    }
}
