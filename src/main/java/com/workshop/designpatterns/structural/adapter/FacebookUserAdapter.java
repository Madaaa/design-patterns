package com.workshop.designpatterns.structural.adapter;

public class FacebookUserAdapter implements InternalUser{
    private FacebookUser facebookUser;

    public FacebookUserAdapter(FacebookUser facebookUser) {
        this.facebookUser = facebookUser;
    }

    @Override
    public String getUserName() {
        return facebookUser.getFirstName().concat(facebookUser.getLastName());
    }
}
