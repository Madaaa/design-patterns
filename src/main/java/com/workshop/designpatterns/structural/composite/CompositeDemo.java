package com.workshop.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class CompositeDemo {
    public static void main(String[] args) {
        Directory directory = new Directory("dir1");

        directory.addComponent(new File("File1"));
        directory.addComponent(new File("File2"));
        directory.addComponent(new File("File3"));

        List<Component> components = new ArrayList<>();
        components.add(new File("File4"));

        Directory secondDirectory = new Directory("dir2");
        secondDirectory.setComponents(components);
        directory.addComponent(secondDirectory);

        directory.printName();
    }
}
