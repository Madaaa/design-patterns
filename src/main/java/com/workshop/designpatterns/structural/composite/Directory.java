package com.workshop.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Directory extends Component{
    List<Component> components = new ArrayList<>();

    Directory(String name) {
        this.name = name;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    void printName() {
        Iterator<Component> iterator = components.iterator();

        System.out.println("------->"+ this.name + "<--------");

        while (iterator.hasNext()) {
            iterator.next().printName();
        }
    }
}
