package com.workshop.designpatterns.structural.composite;

public class File extends Component {
    File(String name){
        this.name = name;
    }
    void printName() {
        System.out.println(this.name);
    }
}
