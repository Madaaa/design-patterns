package com.workshop.designpatterns.structural.composite;

public abstract class Component {
    protected String name;

    abstract void printName();
}
