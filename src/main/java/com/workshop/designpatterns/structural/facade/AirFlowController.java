package com.workshop.designpatterns.structural.facade;

/**
 * source code http://www.baeldung.com/java-facade-pattern
 */

public class AirFlowController {
    void takeAir() {}
    void off() {}
}
