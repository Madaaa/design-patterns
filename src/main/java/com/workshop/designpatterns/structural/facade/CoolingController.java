package com.workshop.designpatterns.structural.facade;

/**
 * source code http://www.baeldung.com/java-facade-pattern
 */

public class CoolingController {
    void run(){}
    void cool(int limit){}
    void stop(){}
    void setTemperatureUpperLimit(int degrees){}
}
