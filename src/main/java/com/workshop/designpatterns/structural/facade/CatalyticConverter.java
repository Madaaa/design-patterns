package com.workshop.designpatterns.structural.facade;

/**
 * source code http://www.baeldung.com/java-facade-pattern
 */

public class CatalyticConverter {
    void on(){}
    void off(){}
}
