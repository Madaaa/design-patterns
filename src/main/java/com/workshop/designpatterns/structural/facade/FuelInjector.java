package com.workshop.designpatterns.structural.facade;

/**
 * source code http://www.baeldung.com/java-facade-pattern
 */

public class FuelInjector {
    void on(){}
    void inject(){}
    void off(){}
}
