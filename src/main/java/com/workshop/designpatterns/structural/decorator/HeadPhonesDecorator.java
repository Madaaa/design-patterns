package com.workshop.designpatterns.structural.decorator;

public class HeadPhonesDecorator extends PhoneDecorator {
    private static int HEAD_PHONES_PRICE = 100;

    public HeadPhonesDecorator(PhoneComponent phoneComponent) {
        super(phoneComponent);
    }


    public int getPrice() {
        return super.getPrice() + HEAD_PHONES_PRICE;
    }
}
