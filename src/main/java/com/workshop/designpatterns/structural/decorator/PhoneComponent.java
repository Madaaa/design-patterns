package com.workshop.designpatterns.structural.decorator;

public interface PhoneComponent {
    int getPrice();
}
