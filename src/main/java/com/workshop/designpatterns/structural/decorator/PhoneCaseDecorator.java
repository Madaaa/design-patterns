package com.workshop.designpatterns.structural.decorator;

public class PhoneCaseDecorator extends PhoneDecorator {
    private static int PHONE_CASE_PRICE = 100;

    public PhoneCaseDecorator(PhoneComponent phoneComponent) {
        super(phoneComponent);
    }


    public int getPrice() {
       return super.getPrice() + PHONE_CASE_PRICE;
    }
}
