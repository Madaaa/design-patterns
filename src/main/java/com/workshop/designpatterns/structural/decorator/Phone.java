package com.workshop.designpatterns.structural.decorator;

public class Phone implements PhoneComponent {
    private int price;

    public Phone(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
