package com.workshop.designpatterns.structural.decorator;

public class DecoratorDemo {
    public static void main(String[] args) {
        PhoneComponent phoneComponent = new PhoneCaseDecorator(new HeadPhonesDecorator(new Phone(1_500)));
        System.out.println(phoneComponent.getPrice());
    }
}
