package com.workshop.designpatterns.structural.decorator;

public class PhoneDecorator implements PhoneComponent {
    private PhoneComponent phoneComponent;

    public PhoneDecorator(PhoneComponent phoneComponent) {
        this.phoneComponent = phoneComponent;
    }

    public int getPrice() {
        return this.phoneComponent.getPrice();
    }
}
