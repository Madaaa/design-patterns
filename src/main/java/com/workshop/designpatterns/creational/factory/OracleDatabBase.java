package com.workshop.designpatterns.creational.factory;

public class OracleDatabBase implements DataBase{
    public void setConnection() {
        System.out.println("Oracle DataBase connection set");
    }
}
