package com.workshop.designpatterns.creational.factory;

public class FactoryDemo {
    public static void main(String[] args) {
        DataBase dataBase = DataBaseFactory.getDataBase("MySql");
        dataBase.setConnection();
    }
}
