package com.workshop.designpatterns.creational.factory;

public class DataBaseFactory {
    static DataBase getDataBase(String dataBaseType) {
        DataBase dataBase;

        switch (dataBaseType) {
            case("MySql"):
                dataBase = new MySQLDataBase();
                break;
            case("Oracle"):
                dataBase = new OracleDatabBase();
                break;
            default:
                throw new IllegalArgumentException("Invalid dataBaseType: " + dataBaseType);
        }

        return dataBase;
    }
}
