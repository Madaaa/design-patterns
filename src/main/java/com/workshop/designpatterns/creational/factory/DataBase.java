package com.workshop.designpatterns.creational.factory;

public interface DataBase {
    void setConnection();
}
