package com.workshop.designpatterns.creational.factory;

public class MySQLDataBase implements DataBase {
    public void setConnection() {
        System.out.println("MySQL DataBase connection set");
    }
}
