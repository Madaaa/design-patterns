package com.workshop.designpatterns.creational.prototype;

import java.util.Objects;

public class Person {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String cnp;

    public Person() { }

    public Person(Person targetPerson) {
        this.firstName = targetPerson.firstName;
        this.lastName = targetPerson.lastName;
        this.phoneNumber = targetPerson.phoneNumber;
        this.cnp = targetPerson.cnp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public Person clone(){
        return new Person(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) &&
                Objects.equals(lastName, person.lastName) &&
                Objects.equals(phoneNumber, person.phoneNumber) &&
                Objects.equals(cnp, person.cnp);
    }
}
