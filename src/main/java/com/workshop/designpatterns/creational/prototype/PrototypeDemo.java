package com.workshop.designpatterns.creational.prototype;

public class PrototypeDemo {
    public static void main(String[] args) {
        Person person = new Person();
        person.setFirstName("John");
        person.setLastName("Doe");
        person.setPhoneNumber("0712312312");
        person.setCnp("212332323232");

        Person anotherPerson = person.clone();

        System.out.println(person.equals(anotherPerson));
    }
}
