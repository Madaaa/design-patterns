package com.workshop.designpatterns.creational.builder;

/** source https://sourcemaking.com/design_patterns/builder/java/2
 *
 */


class HawaiianPizzaBuilder extends PizzaBuilder {
    public void buildDough() {
        pizza.setDough("cross");
    }

    public void buildSauce() {
        pizza.setSauce("mild");
    }

    public void buildTopping() {
        pizza.setTopping("ham+pineapple");
    }
}