package com.workshop.designpatterns.creational.builder;

/** source https://sourcemaking.com/design_patterns/builder/java/2
 *
 */


class SpicyPizzaBuilder extends PizzaBuilder {
    public void buildDough() {
        pizza.setDough("pan baked");
    }

    public void buildSauce() {
        pizza.setSauce("hot");
    }

    public void buildTopping() {
        pizza.setTopping("pepperoni+salami");
    }
}