package com.workshop.designpatterns.creational.singleton;

public class Singleton {
    private static  Singleton instance;

    private Singleton() {}

    public static synchronized Singleton getInstance(String value) {
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;
    }
}
