package com.workshop.designpatterns.creational.userbuilder;

public class User {
    private double birthdayDiscount;
    private boolean hasAccessToSelectedProducts;
    private boolean receivesPromotions;

    public double getBirthdayDiscount() {
        return birthdayDiscount;
    }

    public void setBirthdayDiscount(double birthdayDiscount) {
        this.birthdayDiscount = birthdayDiscount;
    }

    public boolean isHasAccessToSelectedProducts() {
        return hasAccessToSelectedProducts;
    }

    public void setHasAccessToSelectedProducts(boolean hasAccessToSelectedProducts) {
        this.hasAccessToSelectedProducts = hasAccessToSelectedProducts;
    }

    public boolean isReceivesPromotions() {
        return receivesPromotions;
    }

    public void setReceivesPromotions(boolean receivesPromotions) {
        this.receivesPromotions = receivesPromotions;
    }

    @Override
    public String toString() {
        return "User{" +
                "birthdayDiscount=" + birthdayDiscount +
                ", hasAccessToSelectedProducts=" + hasAccessToSelectedProducts +
                ", receivesPromotions=" + receivesPromotions +
                '}';
    }
}
