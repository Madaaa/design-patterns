package com.workshop.designpatterns.creational.userbuilder;

public class PremiumUserBuilder extends UserBuilder {

    @Override
    public void setBirthdayDiscount() {
        this.user.setBirthdayDiscount(0.5);
    }

    @Override
    public void setHasAccessToSelectedProducts() {
        this.user.setHasAccessToSelectedProducts(true);
    }

    @Override
    public void setReceivesPromotions() {
        this.user.setReceivesPromotions(true);
    }
}
