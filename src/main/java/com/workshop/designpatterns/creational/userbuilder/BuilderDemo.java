package com.workshop.designpatterns.creational.userbuilder;

public class BuilderDemo {
    public static void main(String[] args) {
        Clerk director  = new Clerk(new PremiumUserBuilder());
        director.createNewUser();
        System.out.println(director.getUser());

        director = new Clerk(new StandardUserBuilder());
        director.createNewUser();
        System.out.println(director.getUser());
    }
}
