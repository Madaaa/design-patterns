package com.workshop.designpatterns.creational.userbuilder;

public class StandardUserBuilder extends UserBuilder {
    @Override
    public void setBirthdayDiscount() {
        this.user.setBirthdayDiscount(0.3);
    }

    @Override
    public void setHasAccessToSelectedProducts() {
        this.user.setHasAccessToSelectedProducts(false);
    }

    @Override
    public void setReceivesPromotions() {
        this.user.setReceivesPromotions(true);
    }
}
