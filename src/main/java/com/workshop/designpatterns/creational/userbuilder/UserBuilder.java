package com.workshop.designpatterns.creational.userbuilder;

public abstract class UserBuilder {
    protected User user;

    void createNewUser() { this.user = new User(); }

    public User getUser() {
        return user;
    }

    public abstract void setBirthdayDiscount();
    public abstract void setHasAccessToSelectedProducts();
    public abstract void setReceivesPromotions();
}
