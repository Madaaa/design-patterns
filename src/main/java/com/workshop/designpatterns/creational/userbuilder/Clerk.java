package com.workshop.designpatterns.creational.userbuilder;

public class Clerk {
    private UserBuilder userBuilder;

    public Clerk(UserBuilder userBuilder) {
        this.userBuilder = userBuilder;
    }

    public User getUser() {
        return userBuilder.getUser();
    }

    public void createNewUser() {
        userBuilder.createNewUser();
        userBuilder.setBirthdayDiscount();
        userBuilder.setHasAccessToSelectedProducts();
        userBuilder.setReceivesPromotions();
    }
}
