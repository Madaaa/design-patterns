package com.workshop.designpatterns.behavioral.observer;

public class Gates implements  AlarmListner {

    public void alarm() {
        System.out.println("Gates open");
    }
}
