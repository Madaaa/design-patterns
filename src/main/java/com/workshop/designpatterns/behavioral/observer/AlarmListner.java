package com.workshop.designpatterns.behavioral.observer;

public interface AlarmListner {
    void alarm();
}
