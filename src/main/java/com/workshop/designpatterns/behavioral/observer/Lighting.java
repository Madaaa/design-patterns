package com.workshop.designpatterns.behavioral.observer;

public class Lighting implements AlarmListner {

    public void alarm() {
        System.out.println("Lights up");
    }
}
