package com.workshop.designpatterns.behavioral.observer;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SensorSystem {
    private List<AlarmListner> alarmListners = new ArrayList<>();

    public void registerListener(AlarmListner alarmListner) {
        alarmListners.add(alarmListner);
    }

    public void soundTheAlarm() {
        Iterator<AlarmListner> iterator = alarmListners.iterator();

        while(iterator.hasNext()) {
            iterator.next().alarm();
        }
    }
}
