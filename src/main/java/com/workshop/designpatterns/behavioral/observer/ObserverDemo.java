package com.workshop.designpatterns.behavioral.observer;

public class ObserverDemo {
    public static void main(String[] args) {
        SensorSystem sensorSystem = new SensorSystem();
        sensorSystem.registerListener(new Lighting());
        sensorSystem.registerListener(new Gates());
        sensorSystem.soundTheAlarm();
    }
}
