package com.workshop.designpatterns.behavioral.visitor;

public interface Visitor {
    public void visit(Book book);
    public void visit(Phone phone);
}
