package com.workshop.designpatterns.behavioral.visitor;

public class DiscountVisitor implements Visitor {
    private static final double BOOK_DISCOUNT = 0.2;
    private static final double PHONE_DISCOUNT = 0.05;

    private int totalDiscount;

    public int getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(int totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public void visit(Book book) {
        this.totalDiscount += book.getPrice() * BOOK_DISCOUNT;
    }


    public void visit(Phone phone) {
        this.totalDiscount += phone.getPrice() * PHONE_DISCOUNT;
    }
}
