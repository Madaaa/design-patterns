package com.workshop.designpatterns.behavioral.visitor;

public interface Visitable {
    void acceptVisit(Visitor visitor);
}
