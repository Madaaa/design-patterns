package com.workshop.designpatterns.behavioral.visitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VisitorDemo {
    public static void main(String[] args) {
        List<Visitable> items = new ArrayList<>();
        items.add(new Book(50));
        items.add(new Phone(1_500));

        DiscountVisitor discountVisitor = new DiscountVisitor();
        Iterator<Visitable> iterator = items.iterator();
        while(iterator.hasNext()) {
            iterator.next().acceptVisit(discountVisitor);
        }

        System.out.println(discountVisitor.getTotalDiscount());
    }
}
