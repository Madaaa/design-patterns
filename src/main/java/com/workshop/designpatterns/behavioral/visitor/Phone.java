package com.workshop.designpatterns.behavioral.visitor;

public class Phone implements Visitable {
    private int price;

    public Phone(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void acceptVisit(Visitor visitor) {
        visitor.visit(this);
    }
}
