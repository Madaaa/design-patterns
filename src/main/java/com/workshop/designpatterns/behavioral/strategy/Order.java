package com.workshop.designpatterns.behavioral.strategy;

public class Order {
    private double total;
    private BillingStrategy billingStrategy;

    public Order(double total) {
        this.total = total;
        this.billingStrategy = new NormalStrategy();
    }

    public Order(double total, BillingStrategy billingStrategy) {
        this.total = total;
        this.billingStrategy = billingStrategy;
    }

    public BillingStrategy getBillingStrategy() {
        return billingStrategy;
    }

    public void setBillingStrategy(BillingStrategy billingStrategy) {
        this.billingStrategy = billingStrategy;
    }

    public double getTotal() {
        return this.billingStrategy.getBill(total);
    }
}
