package com.workshop.designpatterns.behavioral.strategy;

public class StrategyDemo {
    public static void main(String[] args) {
        Order order = new Order(100);
        order.setBillingStrategy(new SummerSaleStrategy());
        System.out.println(order.getTotal());
    }
}
