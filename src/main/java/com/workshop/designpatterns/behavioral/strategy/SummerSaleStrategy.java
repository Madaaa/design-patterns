package com.workshop.designpatterns.behavioral.strategy;

public class SummerSaleStrategy implements BillingStrategy {
    private static final double DISCOUNT = 0.2;

    @Override
    public double getBill(double price) {
        return price - price * DISCOUNT;
    }
}
