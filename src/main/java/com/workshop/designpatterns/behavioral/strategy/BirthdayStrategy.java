package com.workshop.designpatterns.behavioral.strategy;

public class BirthdayStrategy implements BillingStrategy {
    private static final double DISCOUNT = 0.1;
    @Override
    public double getBill(double price) {
        return price - price * DISCOUNT;
    }
}
