package com.workshop.designpatterns.behavioral.strategy;

public interface BillingStrategy {
    double getBill(double price);
}
