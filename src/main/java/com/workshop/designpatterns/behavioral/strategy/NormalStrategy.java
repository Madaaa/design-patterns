package com.workshop.designpatterns.behavioral.strategy;

public class NormalStrategy implements BillingStrategy {
    @Override
    public double getBill(double price) {
        return price;
    }
}
