package com.workshop.designpatterns.behavioral.command;


//Invoker or Director
public class RemoteControl {
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void sendCommand() {
        command.execute();
    }
}
