package com.workshop.designpatterns.behavioral.command;

//Client
public class CommandDemo {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();
        Alexa alexa = new Alexa();
        Command turnOnTV = new TurnOnTVCommand(alexa);
        Command turnOffTV = new TurnOffTVCommand(alexa);

        remoteControl.setCommand(turnOnTV);
        remoteControl.sendCommand();

        remoteControl.setCommand(turnOffTV);
        remoteControl.sendCommand();
    }
}
