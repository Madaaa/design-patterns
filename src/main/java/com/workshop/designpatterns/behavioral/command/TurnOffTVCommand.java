package com.workshop.designpatterns.behavioral.command;

//Concrete command
public class TurnOffTVCommand  implements Command{
    private Alexa alexa;

    public TurnOffTVCommand(Alexa alexa) {
        this.alexa = alexa;
    }

    @Override
    public void execute() {
        alexa.turnOffTV();
    }
}
