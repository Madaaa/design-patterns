package com.workshop.designpatterns.behavioral.command;

//Receiver
public class Alexa {
    private boolean isTVOn;

    public void turnOnTV(){
        this.isTVOn = true;
    }

    public void turnOffTV(){
        this.isTVOn = false;
    }
}
