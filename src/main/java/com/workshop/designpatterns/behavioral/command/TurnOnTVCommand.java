package com.workshop.designpatterns.behavioral.command;


//Concrete command
public class TurnOnTVCommand implements Command {
    private Alexa alexa;

    public TurnOnTVCommand(Alexa alexa) {
        this.alexa = alexa;
    }

    @Override
    public void execute() {
        alexa.turnOnTV();
    }
}
