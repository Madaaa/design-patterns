package com.workshop.designpatterns.behavioral.command;

//Command
public interface Command {
    void execute();
}
